#!/usr/bin/env python3
"""
..
    Copyright: 2017 Twinleaf LLC
    Author: kornack@twinleaf.com
    Modified by Vincent Dumont

Log data!

"""

import tldevice
import argparse
import time
import numpy
import struct
import datetime


parser = argparse.ArgumentParser(prog='vmr_log_ntp', 
                                 description='Very simple logging utility.')

parser.add_argument("url", 
                    nargs='?', 
                    default='tcp://localhost/',
                    help='URL: tcp://localhost')
parser.add_argument("--cmd", 
                    action='append', 
                    default=[],
                    type=lambda kv: kv.split(":"), 
                    help='Commands to be run on start; rpc:val')
args = parser.parse_args()

device = tldevice.Device(url=args.url, commands=args.cmd)
logfile = 'log_%s.bin'%datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d-%H%M%S')
print('recording data to %s, press CTRL+C to stop...'%logfile)
file = open(logfile,'wb') 
try:
  for row in device.data.stream_iter():
    floats = numpy.hstack([time.time(),row[:3]])
    s = struct.pack('d'*len(floats), *floats)
    file.write(s)
except KeyboardInterrupt:
  file.close()
  print('\nstreaming ended by user!')
  
with open(logfile,'rb') as f:
  data = f.read()
  size = int(len(data)/32)
  data = struct.unpack('dddd'*size,data)
  data = numpy.reshape(data,(size,4))
date0 = datetime.datetime.fromtimestamp(data[0,0]).strftime('%Y-%m-%d %H:%M:%S.%f')
date1 = datetime.datetime.fromtimestamp(data[-1,0]).strftime('%Y-%m-%d %H:%M:%S.%f')
print(len(data),'data points were streamed from %s to %s'%(date0,date1))
print(data)
