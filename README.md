# Introduction

The protocol to connect, stream and record data from the VMR sensors has been greatly improved since the old VM4 sensors. TwinLeaf has built some very useful tools publicly [available on GitHub](https://github.com/twinleaf). Below we present the 3 tools to be used to test, calibrate and record streaming data.

# Data Visualisation & Calibration

[**Access GitHub repository**](https://github.com/twinleaf/tio-labview)



# Proxy Connection 

[**Access GitHub repository**](https://github.com/twinleaf/tio-tools)

The tools available in this repository were built to communicate between the computer and the Twinleaf sensors. In our CityMag project, we will essentially be using the `proxy` executable:

``` bash
./proxy /dev/cu.usbserial-DM00EHSD
```

# Data handling

[**Access GitHub repository**](https://github.com/twinleaf/tio-python)

## Monitoring



``` bash
tio_monitor.py
```

## Configuring

``` bash
[~]% itio.py
Twinleaf VMR R8 [VMR014]

Use   : vmr.<tab>
In [1]: vmr.gmr(2)
Out[1]:
[[9243.6953125, 9205.146484375],
 [50277.40234375, 50338.0234375],
 [-22914.677734375, -22925.0859375]]

In [2]: vmr.dev.port.boot_mode()
Out[2]: 0

In [3]: vmr.gmr.data.autocutoff()
Out[3]: 1

In [4]: vmr.gmr.data.decimation()
Out[4]: 4

In [5]: vmr.dev.conf.save()

In [6]: exit
vmr thanks you.
[~]%
```

## Recording

``` bash
tio_log.py
```
